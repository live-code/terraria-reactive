import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth.guard';

const routes: Routes = [
  { path: 'rxjs1/:id', loadChildren: () => import('./features/rxjs1/rxjs1.module').then(m => m.Rxjs1Module) },
  { path: 'rxjs2', loadChildren: () => import('./features/rxjs2/rxjs2.module').then(m => m.Rxjs2Module) },
  { path: 'rxjs2/:id', loadChildren: () => import('./features/rxjs2/rxjs2.module').then(m => m.Rxjs2Module) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'catalog', canActivate: [AuthGuard], loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'reactive-forms1', loadChildren: () => import('./features/reactive-forms1/reactive-forms1.module').then(m => m.ReactiveForms1Module) },
  { path: 'reactive-forms2-nested', loadChildren: () => import('./features/reactive-forms2-nested/reactive-forms2-nested.module').then(m => m.ReactiveForms2NestedModule) },
  { path: 'reactive-forms3', loadChildren: () => import('./features/reactive-forms3/reactive-forms3.module').then(m => m.ReactiveForms3Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
