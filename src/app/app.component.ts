import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs';
import { AuthService } from './core/auth.service';

@Component({
  selector: 'te-root',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="rxjs1/1">rxjs 1</button>
    <button routerLink="rxjs2">rxjs 2</button>
    <button routerLink="reactive-forms1">form 1</button>
    <button routerLink="reactive-forms2-nested">form 2</button>
    <button routerLink="reactive-forms3">form 3</button>
    <button
      *teIfRoleIs="'admin'"
      routerLink="catalog">catalog</button>
    <button *teIfLogged (click)="logouthandler()">quit</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'terraria-rxjs';

  constructor(public authService: AuthService, private router: Router) {}

  logouthandler() {
    this.authService.logout();
    this.router.navigateByUrl('login')
  }
}
