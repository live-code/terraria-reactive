import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../auth.service';
import { distinctUntilChanged } from 'rxjs';

@Directive({
  selector: '[teIfLogged]'
})
export class IfLoggedDirective {

  constructor(
    private auth: AuthService,
    private template: TemplateRef<any>,
    private view: ViewContainerRef
  ) {

    this.auth.isLogged$
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(isLogged => {
        if (isLogged) {
          view.createEmbeddedView(template);
        } else {
          view.clear()
        }
      })

  }

}
