import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../auth.service';
import { distinctUntilChanged, withLatestFrom } from 'rxjs';

@Directive({
  selector: '[teIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() teIfRoleIs: string | null = null

  constructor(
    private auth: AuthService,
    private template: TemplateRef<any>,
    private view: ViewContainerRef
  ) {

    this.auth.isLogged$
      .pipe(
        distinctUntilChanged(),
        withLatestFrom(this.auth.role$),
      )
      .subscribe(([isLogged, role]) => {
        if (isLogged && role === this.teIfRoleIs) {
          view.createEmbeddedView(template);
        } else {
          view.clear()
        }
      })

  }

}
