import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {
  catchError,
  concatMap,
  first,
  mergeMap,
  Observable,
  of,
  switchMap,
  take,
  throwError,
  withLatestFrom
} from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        // take(1),
        first(),
        withLatestFrom(this.authService.token$),
        concatMap(([isLogged, tk]) => {
          let cloneReq = request;
          if (isLogged) {
            cloneReq = request.clone({
              setHeaders: { Authorization: 'Bearer ' + tk}
            })
          }
          return next.handle(cloneReq);
        }),
        catchError(err => {
          console.log('sono nell interceptor errore')
          return throwError(err)
        })
      )
  }
}
