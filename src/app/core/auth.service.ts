import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, map, observable, Observable, share, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Auth } from './auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data$ = new BehaviorSubject<Auth | null>(null);

  constructor(private http: HttpClient) {
    const data: string | null = localStorage.getItem('auth');
    if (data) {
      this.data$.next(JSON.parse(data) as Auth)
    }
  }

  login() {
    this.http.get<Auth>('http://localhost:3000/login')
      .subscribe(res => {
        this.data$.next(res);
        localStorage.setItem('auth', JSON.stringify(res))
      })
  }

  logout() {
    this.data$.next(null);
    localStorage.removeItem('auth')
  }

  get role$(): Observable<string | null> {
    return this.data$
      .pipe(
        map(auth => auth ? auth.role : null)
      )
  }

  get token$(): Observable<string | null> {
    return this.data$
      .pipe(
        map(auth => auth ? auth.token : null)
      )
  }

  get isLogged$(): Observable<boolean> {
    return this.data$
      .pipe(
        map(auth => !!auth)
      )
  }
}
