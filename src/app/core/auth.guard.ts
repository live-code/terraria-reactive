import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { map, Observable, tap } from 'rxjs';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private http: HttpClient) {
  }
  canActivate(): Observable<boolean> {
    // async example
   /* return this.http.get<{ success: string }>('http://localhost:3000/validateToken')
      .pipe(
        map(res => res.success === 'ok'),
      )*/
    return this.authService.isLogged$
      .pipe(
        tap((value) => {
          if (!value) {
            this.router.navigateByUrl('login')
          }
        })
      );
  }
  
}
