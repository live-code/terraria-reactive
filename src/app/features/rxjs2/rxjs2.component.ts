import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  catchError,
  combineLatest,
  debounceTime,
  fromEvent,
  interval,
  map,
  Observable, of,
  startWith,
  switchMap,
  withLatestFrom
} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../model/user';

@Component({
  selector: 'te-rxjs2',
  template: `
    <hr>
    <input type="text" [formControl]="input" >
    <h1 *ngIf="(meteo$ | async) as meteo">
      {{meteo?.main.temp}}°
    </h1>
    
    <button routerLink="../2">2</button>
    <button routerLink="../3">3</button>
  `,
})
export class Rxjs2Component {
  input = new FormControl('');

  meteo$: Observable<any> = this.input.valueChanges
    .pipe(
      debounceTime(1000),
      switchMap(
        text => this.http.get(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
          .pipe(
            catchError(() => of(null))
          )
      )
    )


  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute
  ) {

  }


}
