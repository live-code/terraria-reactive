import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Rxjs2Component } from './rxjs2.component';

const routes: Routes = [{ path: '', component: Rxjs2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Rxjs2RoutingModule { }
