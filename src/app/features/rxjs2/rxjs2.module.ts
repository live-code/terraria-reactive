import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Rxjs2RoutingModule } from './rxjs2-routing.module';
import { Rxjs2Component } from './rxjs2.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    Rxjs2Component
  ],
  imports: [
    CommonModule,
    Rxjs2RoutingModule,
    SharedModule
  ]
})
export class Rxjs2Module { }
