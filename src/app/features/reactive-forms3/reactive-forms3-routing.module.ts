import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveForms3Component } from './reactive-forms3.component';

const routes: Routes = [{ path: '', component: ReactiveForms3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReactiveForms3RoutingModule { }
