import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveForms3RoutingModule } from './reactive-forms3-routing.module';
import { ReactiveForms3Component } from './reactive-forms3.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    ReactiveForms3Component
  ],
  imports: [
    CommonModule,
    ReactiveForms3RoutingModule,
    SharedModule
  ]
})
export class ReactiveForms3Module { }
