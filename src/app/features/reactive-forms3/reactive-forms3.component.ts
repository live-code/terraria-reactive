import { Component, Injectable, OnInit } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { interval, map, of, switchMap, timer } from 'rxjs';

const URL = 'https://jsonplaceholder.typicode.com';


@Component({
  selector: 'te-reactive-forms3',
  template: `


    <p class="text-danger" *ngIf="form.get('name')?.errors?.['required']">
      Too short. Min 3 characters
    </p>

    <p class="text-danger" *ngIf="form.get('name')?.errors?.['notAvailable']">
      Username Not available
    </p>
    
    <form [formGroup]="form" (submit)="save()" class="form-group form-inline">
      {{form.get('name')?.pending | json}}
      <input class="form-control"
             type="text"
             formControlName="name"
             placeholder="Search username"
      >

      <div 
        formArrayName="items"
        *ngFor="let item of items?.controls; let i = index; let last = last"
      >
        <div [formGroupName]="i">
          <i class="fa fa-check" *ngIf="item.valid"></i>
          <input type="text" placeholder="item" formControlName="name">
          <input type="text" placeholder="cost" formControlName="cost">
          <i class="fa fa-plus" *ngIf="last && item.valid" (click)="addItem()"></i>
          <i class="fa fa-trash" *ngIf="items.controls.length > 1" (click)="removeItem(i)"></i>
        </div>
      </div>
      
      
      <hr>
      <button class="btn btn-primary" type="submit" [disabled]="form.invalid || form.pending" >
        <span *ngIf="form.pending" class="spinner-border spinner-border-sm" ></span>
        CONFIRM
      </button>
      
    </form>


    <hr>
  `,
  styles: [
  ]
})
export class ReactiveForms3Component implements OnInit {
  form: FormGroup;
  items: FormArray;

  constructor(private fb: FormBuilder, userValidator: UserAsyncValidator) {
    this.form = fb.group({
      name: ['', Validators.required, userValidator.check()],
      items: fb.array([])
    })

    this.items = (this.form.get('items') as FormArray);

    const res = {
      name: 'pippo',
      items: [
        { name: 'a', cost: 1},
        { name: 'b', cost: 2},
      ]
    }

    res.items.forEach(item => {
      this.addItem();
    })

    this.form.setValue(res)
  }


  ngOnInit(): void {
    this.form.addControl('dynamic', this.fb.control('abc'))
  }


  addItem() {
    this.items.push(
      this.fb.group({
        name: ['', Validators.required],
        cost: ['', Validators.required],
      })
    )
  }
  removeItem(index: number) {
    this.items.removeAt(index)
  }

  save() {
    console.log(this.form)
  }


}

/// ---------
@Injectable({
  providedIn: 'root'
})
class UserAsyncValidator {
  constructor(private http: HttpClient) {}

  check(): AsyncValidatorFn {
    return (control) => {
      return timer(1000)
        .pipe(
          switchMap(() => this.http.get<User[]>(`${URL}/users?username=${control.value}`)),
          map(res => res.length ? { notAvailable: true } : null)
        )
    }

  }
}
