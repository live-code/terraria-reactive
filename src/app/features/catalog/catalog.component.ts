import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'te-catalog',
  template: `
    <p>
      catalog works!
    </p>
    <div *ngIf="error">ahia!</div>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {
error: boolean = false;
  constructor(private http: HttpClient) {
    http.get('http://localhost:3000/usersX')
      .subscribe(
        res => console.log(res),
        err => {
          this.error = true;
          console.log('errore!')
        }
      )
  }

  ngOnInit(): void {
  }

}
