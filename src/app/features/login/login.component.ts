import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'te-login',
  template: `
    
    {{f.valid}}
    {{usernameRef.errors | json}}

    <form #f="ngForm" (submit)="loginHandler(f.value)">
      <input type="text" ngModel name="username"
             minlength="3" required 
             teUsernameValidator
             #usernameRef="ngModel"
             #myInput="userValidator" >
      <input type="text" ngModel name="password">
      <button type="submit">submit</button>
    </form>
  `,
})
export class LoginComponent  {
  @ViewChild('myInput') myInput!: any

  constructor(public authService: AuthService) {

    setTimeout(() => {
      this.authService.role$
        .subscribe(console.log);
    }, 2000)
  }

  ngAfterViewInit() {
    console.log(this.myInput.state)
  }

  loginHandler(form: any) {
    console.log(form)
    this.authService.login()
  }
}
