import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';

@Component({
  selector: 'te-reactive-forms2-nested',
  template: `
    
    {{label}}
    <te-my-input2 type="text" [(ngModel)]="label"></te-my-input2>
    
    <pre>{{form.value | json}}</pre>
    <pre>{{form.dirty | json}}</pre>
    <pre>{{form.touched | json}}</pre>
    <form [formGroup]="form" (submit)="doSomething()">
      <te-my-input formControlName="country" label="contry"></te-my-input>
      <te-my-input2 formControlName="city" label="city"></te-my-input2>

      <input type="text" class="form-control" 
             placeholder="codice" 
             formControlName="codice"
      >
      
      <te-anagrafica [group]="form" name="anagrafica"></te-anagrafica>
      
      {{form.get('passwords')?.errors | json}}
      <div formGroupName="passwords">
        <h1>Password</h1>
        {{form.get('passwords')?.get('password1')?.errors | json}}
        <input type="text" class="form-control" placeholder="password" formControlName="password1">
        {{form.get('passwords')?.get('password2')?.errors | json}}
        <input type="text" class="form-control" placeholder="ripeti password" formControlName="password2">
      </div>
   
      <button type="submit" [disabled]="form.invalid">save</button>
    </form>
  `,
  styles: [
  ]
})
export class ReactiveForms2NestedComponent  {
  form: FormGroup;
  label = 'pippo'

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      codice: ['', Validators.required],
      country: 'italia',
      city: 'Gorizia',
      anagrafica: fb.group({
        name: ['', Validators.required],
        surname: ['', Validators.required],
      }),
      passwords: fb.group(
        {
          password1: ['', [Validators.required, Validators.minLength(6)]],
          password2: ['', [Validators.required, Validators.minLength(6)]],
        },
        // OLD
        // { validators: (g: FormGroup) => passwordMatchOLD(g, 'password1', 'password2')}
        // with params
        /*
        {
        validators: (g: FormGroup) => {
          return passwordMatch3(g, 'password1', 'password2')
        }}
        */
        // simple
        { validators: passwordMatch}
      )
    })

    setTimeout(() => {
      this.form.patchValue({ country: 'pippo'})
    }, 1000)
  }

  doSomething() {
    console.log(this.form.value)
  }
}

// simple version
function passwordMatch(g: FormGroup): ValidationErrors | null {
  const pass1 = g.controls['password1']
  const pass2 = g.controls['password2']
  if (pass1.value === pass2.value) {
    return null
  } else {
    return { noMatch: true}
  }
}


// with params
function passwordMatch3(g: FormGroup, password1: string, password2: string): ValidationErrors | null {
  const pass1 = g.controls[password1]
  const pass2 = g.controls[password2]

  if (pass1.value !== pass2.value) {
    // if not match
    pass2.setErrors({ ...pass2.errors, invalidPass: true })
    return { myError: true}
  } else {
    // if match
    if (pass2.errors) {
      pass2.setErrors({ ...pass2.errors })
      return { myError: true}
    } else {
      pass2.setErrors(null)
      return null;
    }
  }

}


// old school
function passwordMatchOLD(g: FormGroup, password1: string, password2: string) {
  const pass1 = g.controls[password1]
  const pass2 = g.controls[password2]

  if (pass1.value !== pass2.value) {
    // if not match
    pass2.setErrors({ ...pass2.errors, invalidPass: true })
  } else {
    // if match
    if (pass2.errors) {
      pass2.setErrors({ ...pass2.errors })
    } else {
      pass2.setErrors(null)
    }
  }

}
function isEmpty(obj: any) {
  if (obj) {
    return Object.keys(obj).length === 0;
  }
  return obj;
}
