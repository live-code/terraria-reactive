import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'te-my-input',
  template: `
    <div class="m-3">
      <label>{{label}}</label>
      <input 
        type="text"
        [value]="value"
        (input)="changeHandler($event)"
        (blur)="onTouched()"
        class="form-control" placeholder="xxxx"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MyInputComponent, multi: true}
  ]
})
export class MyInputComponent implements ControlValueAccessor {
  @Input() label!: string;
  value = ''
  onChange!: (text: string) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
   this.value = text
  }

  changeHandler($event: Event) {
    this.onChange(($event.target as HTMLInputElement).value)
  }
}
