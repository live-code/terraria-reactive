import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'te-my-input2',
  template: `
    <div class="m-3">
      <label>{{label}}</label>
      <input 
        type="text"
        [formControl]="input"
        class="form-control" [placeholder]="label"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: MyInput2Component, multi: true}
  ]
})
export class MyInput2Component implements ControlValueAccessor {
  @Input() label!: string;
  input = new FormControl();
  onChange!: (text: string) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }

}
