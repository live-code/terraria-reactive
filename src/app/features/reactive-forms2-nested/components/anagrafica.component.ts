import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'te-anagrafica',
  template: `
    <div [formGroup]="group">
      <div [formGroupName]="name">
        <h1>
          <i class="fa fa-check" *ngIf="group.get(name)?.valid"></i>
          Anagrafica
        </h1>
        <div *ngIf="group.get(name)?.get('name')?.invalid">Name is not valid</div>
        <input type="text" class="form-control" placeholder="name" formControlName="name">
        <input type="text" class="form-control" placeholder="surname" formControlName="surname">
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AnagraficaComponent {
  @Input() group!: FormGroup
  @Input() name!: string;


}
