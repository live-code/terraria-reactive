import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveForms2NestedRoutingModule } from './reactive-forms2-nested-routing.module';
import { ReactiveForms2NestedComponent } from './reactive-forms2-nested.component';
import { SharedModule } from '../../shared/shared.module';
import { AnagraficaComponent } from './components/anagrafica.component';
import { MyInputComponent } from './components/my-input.component';
import { MyInput2Component } from './components/my-input2.component';


@NgModule({
  declarations: [
    ReactiveForms2NestedComponent,
    AnagraficaComponent,
    MyInputComponent,
    MyInput2Component
  ],
  imports: [
    CommonModule,
    ReactiveForms2NestedRoutingModule,
    SharedModule
  ]
})
export class ReactiveForms2NestedModule { }
