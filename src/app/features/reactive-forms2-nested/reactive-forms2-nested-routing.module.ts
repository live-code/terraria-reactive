import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveForms2NestedComponent } from './reactive-forms2-nested.component';

const routes: Routes = [{ path: '', component: ReactiveForms2NestedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReactiveForms2NestedRoutingModule { }
