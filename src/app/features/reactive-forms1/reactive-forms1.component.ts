import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { debounceTime, distinctUntilChanged, filter, fromEvent, switchMap } from 'rxjs';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { EMAIL_REGEX } from '../../shared/forms/username-validator.directive';
import { vatCFValidator } from '../../shared/forms/my-validators';

@Component({
  selector: 'te-reactive-forms1',
  template: `
    <form [formGroup]="form" (submit)="doSomething()">
      <input type="checkbox" formControlName="isCompany"> are you a company?
      <hr>
      <input type="text" formControlName="companyName" placeholder="Company Name">
      <input type="text" formControlName="vatNumber" 
             [placeholder]="form.get('isCompany')?.value ? 'vat number' : 'CF number'">
      
      {{vatNumber.valid}} 
      
     <!-- <div *ngIf="form.get('vatNumber')?.errors as err">
        <div *ngIf="err['required']">required</div>

      </div>-->
      <hr>
     <button type="submit" [disabled]="form.invalid">SAVE</button>
    </form>
  `,
})
export class ReactiveForms1Component  {
  form: FormGroup;
  vatNumber: FormControl;

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      isCompany: true,
      companyName: ['', Validators.required],
      vatNumber: ['', [Validators.required, (c: FormControl) =>  vatCFValidator(c, 11) ]],
    })
    this.vatNumber = this.form.get('vatNumber') as FormControl;

    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if (isCompany) {
          this.setAsCompany()
        } else {
          this.setAsUser();
        }
         this.vatNumber?.updateValueAndValidity()
      })
  }

  setAsCompany() {
    this.form.get('companyName')?.enable();
    const companyValidators = [Validators.required, (c: FormControl) =>  vatCFValidator(c, 11) ]
     this.vatNumber?.setValidators(companyValidators)
  }

  setAsUser() {
    this.form.get('companyName')?.disable();
    const userValidators = [Validators.required, (c: FormControl) =>  vatCFValidator(c, 16) ]
    this.vatNumber?.setValidators(userValidators)
  }


  doSomething() {
    console.log(this.form.value)
  }
}


