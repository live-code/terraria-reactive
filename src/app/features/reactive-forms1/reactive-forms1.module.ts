import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveForms1RoutingModule } from './reactive-forms1-routing.module';
import { ReactiveForms1Component } from './reactive-forms1.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    ReactiveForms1Component
  ],
  imports: [
    CommonModule,
    ReactiveForms1RoutingModule,
    SharedModule
  ]
})
export class ReactiveForms1Module { }
