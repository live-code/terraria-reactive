import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveForms1Component } from './reactive-forms1.component';

const routes: Routes = [{ path: '', component: ReactiveForms1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReactiveForms1RoutingModule { }
