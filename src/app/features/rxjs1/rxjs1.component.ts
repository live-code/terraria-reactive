import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import {
  catchError,
  concatMap, delay,
  forkJoin,
  map,
  Observable,
  of,
  reduce, share,
  switchMap,
  tap,
  toArray,
  withLatestFrom
} from 'rxjs';
import { Post } from '../../model/post';
import { ActivatedRoute } from '@angular/router';
const API = 'https://jsonplaceholder.typicode.com';

@Component({
  selector: 'te-rxjs1',
  template: `
  `,
})
export class Rxjs1Component {
  users: any = {};
  posts: Omit<Post, 'userId'>[] = [];

  postsReq$ = this.http.get<Post[]>(`${API}/posts`)
    .pipe( delay(2000), share() )

  constructor(private http: HttpClient) {
    setTimeout(() => {
      this.postsReq$.subscribe()
    }, 1000)

    this.postsReq$
      .pipe(
        map(posts =>  ({
          posts,  ids: remove_duplicates(posts.map(post => post.userId))
        })),
        switchMap(obj => obj.ids),
        concatMap(userId => http.get<User>(`${API}/users/${userId}`)),
        reduce((acc: { [key: string]: User }, user: User) => {
          acc[user.id] = user;
          return acc;
        }, {}),
        withLatestFrom(this.postsReq$),
        map(([users, posts]) => {
          return posts.map(post => ({...post, user: users[post.userId]}))
        })
      )
      .subscribe(console.log)

    return;

    this.http.get<Post[]>(`${API}/posts`)
      .subscribe(res => {
        const myPosts = res;
        const ids = remove_duplicates(res.map(post => post.userId))

        const all: Observable<User>[] = [];
        ids.forEach(id => {
          all.push(http.get<User>(`${API}/users/${id}`))
        })

        forkJoin(all)
          .subscribe(allUsers => {
            allUsers.forEach(user => {
              this.users[user.id] = user;
            })

            this.posts = myPosts.map(post => {
              const { userId, ...rest} = post
              return {...rest, user: this.users[post.userId]}
            })
          })

      })

  }

}


function remove_duplicates(arr: any[]) {
  let s = new Set(arr);
  let it = s.values();
  return Array.from(it);
}
