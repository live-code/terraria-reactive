import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Rxjs1RoutingModule } from './rxjs1-routing.module';
import { Rxjs1Component } from './rxjs1.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    Rxjs1Component
  ],
  imports: [
    CommonModule,
    Rxjs1RoutingModule,
    SharedModule
  ]
})
export class Rxjs1Module { }
