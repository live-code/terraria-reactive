import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Rxjs1Component } from './rxjs1.component';

const routes: Routes = [{ path: '', component: Rxjs1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Rxjs1RoutingModule { }
