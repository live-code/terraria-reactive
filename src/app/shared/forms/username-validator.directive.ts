import { Directive, forwardRef } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

@Directive({
  selector: '[teUsernameValidator]',
  exportAs: 'userValidator',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => UsernameValidatorDirective),
      multi: true
    }
  ]
})
export class UsernameValidatorDirective implements Validator{
  state = 'pippo';


  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value && !control.value.match(EMAIL_REGEX)) {
      return { usernameNotEmail: true}
    }
    return null;
  }

}
