import { AbstractControl, ValidationErrors } from '@angular/forms';


const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

export function vatCFValidator(control: AbstractControl, requiredLength: number = 11): ValidationErrors | null {
  if (control.value && control.value.length !== requiredLength) {
    return { invalidSize: true }
  }
  if (control.value && !control.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphaNumeric: true}
  }
  return null;
}
